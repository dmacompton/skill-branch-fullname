import express from 'express';
import fullnameCut from './fullnameHelper';

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
  let result;

  if ('fullname' in req.query) {
    const fullname = req.query.fullname;
    result = fullnameCut(fullname);
  }
  
  res.send(result);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
